#include "Window.h"
#include "rect.h"
#include "Enemy.h"


void pollEvents(Window &window, Rect &rect)
{
	SDL_Event event;

	if (SDL_PollEvent(&event))
	{
		rect.pollEvents(event);
		window.pollEvents(event);
	}

}

int main(int argc, char **argv) 
{
	Window window("Trabajo Practico", 800, 600);
	Rect rect(window, 60, 60, 100, 100, "res/Nave.png");

	Enemy enemy(window, 32, 32, 400, 400, "res/Enemy.png");
	Enemy enemy2(window, 32, 32, 300, 300, "res/Enemy.png");
	Enemy enemy3(window, 32, 32, 600, 200, "res/Enemy.png");
	Enemy enemy4(window, 32, 32, 500, 500, "res/Enemy.png");
	Enemy enemy5(window, 32, 32, 350, 250, "res/Enemy.png");
	Enemy enemy6(window, 32, 32, 0, 0, "res/Enemy.png");
	Enemy enemy7(window, 32, 32, 600, 600, "res/Enemy.png");
	Enemy enemy8(window, 32, 32, 700, 400, "res/Enemy.png");
	Enemy enemy9(window, 32, 32, 600, 450, "res/Enemy.png");
	
	while (!window.isClosed())
	{
		pollEvents(window, rect);
		
		enemy.move();
		enemy2.move();
		enemy3.move();
		enemy4.move();
		enemy5.move();
		enemy6.move();
		enemy7.move();
		enemy8.move();
		enemy9.move();
		
		rect.limits();
		
		enemy.limits();
		enemy2.limits();
		enemy3.limits();
		enemy4.limits();
		enemy5.limits();
		enemy6.limits();
		enemy7.limits();
		enemy8.limits();
		enemy9.limits();

		enemy.intersectsWith(rect);
		enemy2.intersectsWith(rect);
		enemy3.intersectsWith(rect);
		enemy4.intersectsWith(rect);
		enemy5.intersectsWith(rect);
		enemy6.intersectsWith(rect);
		enemy7.intersectsWith(rect);
		enemy8.intersectsWith(rect);
		enemy9.intersectsWith(rect);

		rect.draw();

		enemy.draw();
		enemy2.draw();
		enemy3.draw();
		enemy4.draw();
		enemy5.draw();
		enemy6.draw();
		enemy7.draw();
		enemy8.draw();
		enemy9.draw();

		window.clear();
	}

	return 0;
}