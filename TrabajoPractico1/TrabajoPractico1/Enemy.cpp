#include "Enemy.h"
#include <SDL_image.h>
#include <time.h> 
#include <stdio.h>
#include <stdlib.h>  
#include <iostream>


Enemy::Enemy(const Window &window, int w, int h, int x, int y, const std::string &image_path) :
	Window(window), _w(w), _h(h), _x(x), _y(y)
{
	auto surface = IMG_Load(image_path.c_str());

	_texture = SDL_CreateTextureFromSurface(_renderer, surface);

	SDL_FreeSurface(surface);
}

Enemy::~Enemy()
{
	SDL_DestroyTexture(_texture);
}

void Enemy::draw() const
{
	SDL_Rect rect = { _x,_y,_w,_h };

	if (_texture)
	{
		SDL_RenderCopy(_renderer, _texture, nullptr, &rect);
	}
	else
	{
		SDL_SetRenderDrawColor(_renderer, _r, _g, _b, _a);
		SDL_RenderFillRect(_renderer, &rect);
	}
}

void Enemy::limits()
{
	if (_x < 0)
		_x = 800 - _w;

	if (_y < 0)
		_y = 600 - _h;

	if (_x + _w > 800)
		_x = 0;

	if (_y + _h > 600)
		_y = 0;
}

void Enemy::move()
{
	int iSecret;

	//srand(time(NULL));

	iSecret = rand() % 3;

	cont++;

	if (cont > 200)
	{
		switch (iSecret)
		{
		case 0:
			_x -= 10;
			break;
		case 1:
			_x += 10;
			break;
		case 2:
			_y -= 10;
			break;
		case 3:
			_y += 10;
			break;
		}
		
		cont = 0;
	}
}

bool Enemy::intersectsWith(Rect &e)
{
	if ((_x + _w) < e._x || _x > (e._x + e._w) || (_y + _h) < e._y || _y > (e._y + e._h))
	{
		return false;
	}
	else
	{
		std::cout << "Espacio para volver a jugar" << std::endl;
		e.Lose();
		return true;
	}
}