#include "rect.h"
#include <SDL_image.h>


Rect::Rect(const Window &window, int w, int h, int x, int y, int r, int g, int b, int a) :
	Window(window), _w(w), _h(h), _x(x), _y(y), _r(r), _g(g), _b(b), _a(a)
{

}

Rect::Rect(const Window &window, int w, int h, int x, int y, const std::string &image_path) :
	Window(window), _w(w), _h(h), _x(x), _y(y)
{
	auto surface = IMG_Load(image_path.c_str());

	_texture = SDL_CreateTextureFromSurface(_renderer, surface);

	SDL_FreeSurface(surface);
}

Rect::~Rect()
{
	SDL_DestroyTexture(_texture);
}

void Rect::draw() const
{
	SDL_Rect rect = { _x,_y,_w,_h };

	if (_texture)
	{
		SDL_RenderCopy(_renderer, _texture, nullptr, &rect);
	}
	else 
	{
		SDL_SetRenderDrawColor(_renderer, _r, _g, _b, _a);
		SDL_RenderFillRect(_renderer, &rect);
	}
}

void Rect::limits()
{
	if (_x < 0)
		_x = 0;

	if (_y < 0)
		_y = 0;

	if (_x + _w > 800)
		_x = 800- _w;

	if (_y + _h > 600)
		_y = 600 - _h;
}

void Rect::Lose()
{
	SDL_SetTextureColorMod(_texture, 255, 0, 0);
	vmov = 0;
}

void Rect::Restart()
{
	SDL_SetTextureColorMod(_texture, 255, 255, 255);
	vmov = 10;
}

void Rect::pollEvents(SDL_Event &event)
{	
	if (event.type == SDL_KEYDOWN)
	{
		switch (event.key.keysym.sym)
		{
		case SDLK_LEFT:
			_x -= vmov;
			break;
		case SDLK_RIGHT:
			_x += vmov;
			break;
		case SDLK_UP:
			_y -= vmov;
			break;
		case SDLK_DOWN:
			_y += vmov;
			break;
		case SDLK_SPACE:
			Restart();
			break;
		}
	}
}