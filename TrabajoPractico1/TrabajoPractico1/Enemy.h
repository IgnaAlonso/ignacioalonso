#pragma once

#include "Window.h"
#include "rect.h"

class Enemy : public Window
{
public:
	Enemy(const Window &window, int w, int h, int x, int y, const std::string &image_path);
	~Enemy();

	void draw() const;
	void limits();
	void move();
	bool intersectsWith(Rect &e);


	int _w, _h;
	int _x, _y;
	int _r, _g, _b, _a;
	int cont = 0;

private:
	SDL_Texture *_texture = nullptr;
};
